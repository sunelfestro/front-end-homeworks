let menu = document.querySelector('.tabs');
let tabs = document.querySelectorAll('.tabs-title');
let content = document.querySelectorAll('.svc-content');

removeContent(content);
showContent(content, 0);

menu.addEventListener('click', (e) => {
    let tg = e.target;
    removeActiveTab(tabs, "active");

    if (tg.classList.contains("tabs-title")) {
        tabs.forEach((tab, i) => {
            if (tg === tab) {
                removeContent(content);
                tab.classList.add('active');
                content.forEach((item, i) => {
                    if (tg.innerText === content[i].id) {
                        showContent(content, i);
                    }
                })
            }
        })
    }
})