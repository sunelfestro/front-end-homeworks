function removeContent(elem) {
    elem.forEach((item) => {
        item.style.display = 'none';
    })
}

function showContent(elem, i) {
    elem[i].style.display = 'flex';
    for (let childrenKey in elem[i].children){
        if (+childrenKey > 11){
            elem[i].children.item(+childrenKey).style.display = 'none';
        }
    }
}

function removeActiveTab(tb, cls) {
    tb.forEach((elem) => {
        elem.classList.remove(cls);
    })
}

new Glide('.glide', {
    type: 'carousel',
}).mount();