let menuButton = document.querySelector('.menu-button__container');
let btnIcon = document.querySelector('.menu-button__item');
let nav = document.querySelector('.header__nav');

menuButton.addEventListener('click', () => {
    btnIcon.classList.toggle('menu-button__item_active');
    nav.classList.toggle('header__nav_active');
})