const gulp = require("gulp");
const sass = require("gulp-sass");
const prefix = require("gulp-autoprefixer");
const clean = require("gulp-clean");
const cleanCss = require("gulp-clean-css");
const concat = require("gulp-concat");
const sourcemaps = require('gulp-sourcemaps');
const img = require("gulp-image");
const plumber = require("gulp-plumber");
const htmlValidator = require("gulp-w3c-html-validator");
const jsMin = require("gulp-js-minify");
const purgeCss = require("gulp-purgecss");
const rename = require("gulp-rename");
const sync = require("browser-sync").create();

function html() {
    return gulp
        .src("src/index.html")
        .pipe(plumber())
        .pipe(htmlValidator())
        .pipe(gulp.dest("dist"));
}

function cleaner() {
    return gulp.src("dist")
        .pipe(clean())
}

function scss() {
    return gulp.src("src/scss/*.scss")
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefix({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCss())
        .pipe(sourcemaps.write())
        .pipe(rename({
            basename: "styles",
            suffix: ".min"
        }))
        .pipe(gulp.dest('dist'))
}

function js() {

    return gulp.src('src/js/*.js')
        .pipe(plumber())
        .pipe(concat('scripts.js'))
        .pipe(jsMin())
        .pipe(rename({
            basename: "scripts",
            suffix: '.min'
        }))
        .pipe(gulp.dest('dist'))
}

function optImg(done) {
    gulp.src('src/img/*')
        .pipe(img())
        .pipe(gulp.dest('dist/img'));
    done();
}

function purge() {
    return gulp.src("dist/styles.min.css")
        .pipe(purgeCss({
            content: ['dist/index.html']
        }))
        .pipe(gulp.dest('dist'))
}

function readyReload(cb) {
    sync.reload();
    cb();
}

function serve(cb) {
    sync.init({
        server: "dist",
        notify: false,
        open: true,
        cors: true,
    });

    gulp.watch("src/**/*.html", gulp.series(html, readyReload));
    gulp.watch(
        "src/scss/*.scss",
        gulp.series(scss, (cb) =>
            gulp.src("dist").pipe(sync.stream()).on("end", cb)
        )
    );
    gulp.watch(
        "src/js/*.js",
        gulp.series(js, (cb) =>
            gulp.src("dist").pipe(sync.stream()).on("end", cb)
        )
    );

    return cb();
};

const dev = gulp.parallel(html, js, scss);
const build = gulp.series(cleaner, optImg, scss, purge, js, html);

exports.dev = gulp.series(dev, serve);
exports.build = build;