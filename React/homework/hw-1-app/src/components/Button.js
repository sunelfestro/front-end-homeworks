import React, { Component } from "react";

class Button extends Component {

  render(){
    const {text, onClick, backgroundColor} = this.props;

    return (
    <button className="btn" style={{backgroundColor}} onClick={onClick}>{text}</button>
    )

  }
}

export default Button;