import React, {Component} from 'react';
import Modal from "./components/Modal";
import Button from "./components/Button";
import './App.scss';

class App extends Component {
    state = {
        isOpen1: false,
        isOpen2: false,
    }

    toggleModalFirst = () => {
        const modalState = this.state.isOpen1;
        this.setState({isOpen1: !modalState})
    }

    toggleModalSecond = () => {
        const modalState = this.state.isOpen2;
        this.setState({isOpen2: !modalState})
    }

    render() {
        return (
            <>
                <div className='btn-wrapper'>
                    <Button text={"Open first modal"} backgroundColor={'#5a7d9e'} onClick={this.toggleModalFirst}/>
                    <Button text={"Open second modal"} backgroundColor={"#5a9d7e"} onClick={this.toggleModalSecond}/>
                </div>

                {this.state.isOpen1 && <Modal title={"First Modal"}
                                              classHead={"modal__head modal__head_One"}
                                              text={"Congrats, you finally open first modal!"}
                                              isOpen={this.state.isOpen1}
                                              closeButton={true}
                                              closeClick={this.toggleModalFirst}
                                              actions={<div className="foot_btn">
                                                  <button className="modal_btn" onClick={this.toggleModalFirst}>Ok
                                                  </button>
                                                  <button className="modal_btn" onClick={this.toggleModalFirst}>Close
                                                  </button>
                                              </div>}/>}

                {this.state.isOpen2 && <Modal title={"Second modal"}
                                              classHead={"modal__head modal__head_Two"}
                                              text={"Congrats, you finally open second modal!"}
                                              isOpen={this.state.isOpen2}
                                              closeButton={false}
                                              closeClick={this.toggleModalSecond}
                                              actions={<div className="foot_btn">
                                                  <button className="modal_btn" onClick={this.toggleModalSecond}>Apply
                                                  </button>
                                                  <button className="modal_btn"
                                                          onClick={this.toggleModalSecond}>Cancel
                                                  </button>
                                              </div>}/>}
            </>
        )
    }
}


export default App;
