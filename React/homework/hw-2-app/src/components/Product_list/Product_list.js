import React, { Component } from "react";
import Product from "../Product/Product";
import "./Product_list.scss"
import PropTypes from "prop-types";

class ProductList extends Component{
    render() {
        const { products, favorites, addToFavorites, cart, addToCart, removeFromCart } = this.props;

        return (
            <div className="products__container">
                {
                    products.map(elem =>
                        <Product
                            key={elem.id}
                            data={elem}
                            favorites={favorites}
                            addToFavorites={addToFavorites}
                            cart={cart}
                            addToCart={addToCart}
                            removeFromCart={removeFromCart}
                        />)
                }
            </div>
        )
    }
}

ProductList.propTypes = {
    products: PropTypes.array.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired,
    removeFromCart: PropTypes.func.isRequired
}

export default ProductList;