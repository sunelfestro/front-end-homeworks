import React, { Component } from 'react';
import PropTypes from "prop-types";
// import "./Button.scss"

class Button extends Component{
  render() {
    const { text, backgroundColor, handler, className, productId, color, borderColor } = this.props;

    return (
        <button
            className={className}
            style={{backgroundColor: backgroundColor, color: color, borderColor: borderColor}}
            onClick={() => handler(productId)}
        >
          {text}
        </button>
    )
  }
}

Button.propTypes = {
  text: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string,
  handler: PropTypes.func.isRequired,
  className: PropTypes.string,
  productId: PropTypes.number,
  color: PropTypes.string,
  borderColor: PropTypes.string
}

Button.defaultProps = {
  backgroundColor: "brown",
  className: "btn",
  color: "#fff",
  borderColor: "transparent",
  productId: null
}

export default Button;
