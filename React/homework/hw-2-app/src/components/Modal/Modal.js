import React, { Component, Fragment } from 'react';
import './Modal.scss';
import PropTypes from "prop-types";

class Modal extends Component {
    render() {
        const {title, actions, closeClick, text } = this.props;

        return (
            <Fragment>
                <div onClick={
                    (event) =>
                        event.target.children[0] ? event.target.children[0].classList.contains("popup") ? closeClick : null : ''
                } className="popup__wrapper popup__wrapper_active">
                    <div className="popup">
                        <div className="popup__header">
                            <h3 className="popup__caption">{title}</h3>
                            <div onClick={closeClick} className="close"></div>
                        </div>
                        <div className="popup__text">{text}</div>
                        {actions}
                    </div>
                </div>
            </Fragment>
        )
    }
}

Modal.propTypes = {
    closeCLick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    actions: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired
}

export default Modal;
