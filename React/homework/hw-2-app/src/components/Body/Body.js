import React, { Component } from "react";
import axios from "axios";
import "./Body.scss"
import Loader from "../Loader/Loader";
import ProductList from "../Product_list/Product_list";
import PropTypes from "prop-types";

class Body extends Component {
    state = {
        products: [],
        isLoading: true
    }

    componentDidMount() {
        setTimeout(() => {
            axios('Product_Data.json')
                .then(res => this.setState({isLoading: false, products: res.data}));
        }, 2000);
    }

    render() {
        const { products, isLoading } = this.state;
        const { addToCart, addToFavorites, favorites, cart, removeFromCart } = this.props;

        if (isLoading) {
            return <Loader/>
        }

        return (
            <>
                <div className="app__storesCount">
                    <h3>Favorites: {favorites.length}</h3>
                    <h3>Cart: {cart.length}</h3>
                </div>
                <ProductList
                    cart={cart}
                    favorites={favorites}
                    addToFavorites={addToFavorites}
                    addToCart={addToCart}
                    removeFromCart={removeFromCart}
                    products={products}
                />
            </>
        )
    }
}

Body.propTypes = {
    addToCart: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired,
    removeFromCart: PropTypes.func.isRequired
}

export default Body;