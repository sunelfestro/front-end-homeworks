import React, {Component} from "react";
import './ProductStyle.scss';
import Button from "../Button/Button";
import Star from "../Star_icon/Star_icon";
import PropTypes from "prop-types";

class Product extends Component {
    render() {
        const {data, addToFavorites, favorites, cart, addToCart, removeFromCart} = this.props;

        return (
            <>
                <div className="product__wrapper">
                    <div className="product__img_wrapper">
                        <img className="product__img" src={data.image} alt=""/>
                    </div>

                    <div className="favorites">
                        <Star
                            color={favorites.includes(data.id) ? "black" : "#f1f1f1"}
                            onClick={addToFavorites}
                            productId={data.id}
                        />
                    </div>

                    <div className="product__title">{data.title}</div>
                    <div className="product__code"> Code: {data.code}</div>
                    <div className="product__bottom">
                        <div className="product__price">{data.price}</div>
                        <Button
                            className="product__add-to-cart"
                            text={cart.includes(data.id) ? "in cart" : "add to cart"}
                            backgroundColor={cart.includes(data.id) ? "black" : "#f6d4ad"}
                            color={cart.includes(data.id) ? "white" : "black"}
                            borderColor={cart.includes(data.id) ? "transparent" : "black"}
                            handler={cart.includes(data.id) ? removeFromCart : addToCart}
                            productId={data.id}
                        />
                    </div>
                </div>
            </>
        )
    }
}

Product.propTypes = {
    data: PropTypes.object.isRequired,
    addToCart: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    favorites: PropTypes.array.isRequired,
    cart: PropTypes.array.isRequired,
    removeFromCart: PropTypes.func.isRequired
}

export default Product;