import React, {Component} from 'react';
import './App.scss';
import Button from './components/Button/Button';
import Modal from "./components/Modal/Modal";
import Body from "./components/Body/Body";

class App extends Component {
    state = {
        favorites: JSON.parse(localStorage.getItem('favorites')) || [],
        cart: JSON.parse(localStorage.getItem('cart')) || []
    }

    modalHandler = (productId) => {
        this.setState(() => ({
                statusModal: "addingToCart",
                productId
            }
        ))
    };

    modalHandlerRemove = (productId) => {
        this.setState(() => ({
                statusModal: "RemovingFromCart",
                productId
            }
        ))
    };

    close = () => {
        this.setState(() => ({
                statusModal: "closed",
            }
        ))
    }

    toggleFavorites = (productId) => {
        const {favorites} = this.state;

        if (!favorites.includes(productId)) {
            this.setState({favorites: [...favorites, productId]}, () => {
                localStorage.setItem('favorites', JSON.stringify(this.state.favorites));
            });
        } else {
            this.setState({favorites: favorites.filter(el => el !== productId)}, () => {
                localStorage.setItem('favorites', JSON.stringify(this.state.favorites));
            });
        }
    }

    toggleCart = () => {
        const {cart, productId} = this.state;

        if (!cart.includes(productId)) {
            this.setState({cart: [...cart, productId]}, () => {
                localStorage.setItem('cart', JSON.stringify(this.state.cart));
            });
        } else {
            this.setState({cart: cart.filter(el => el !== productId)}, () => {
                localStorage.setItem('cart', JSON.stringify(this.state.cart));
            });
        }

        this.setState({statusModal: false});
    }


    render() {
        const {statusModal, favorites, cart} = this.state;

        return (
            <div className="app">
                <Body
                    addToFavorites={this.toggleFavorites}
                    addToCart={this.modalHandler}
                    removeFromCart={this.modalHandlerRemove}
                    favorites={favorites}
                    cart={cart}
                />

                {
                    statusModal === "addingToCart" ?
                        <Modal
                            title="Adding ot cart"
                            text="Are you sure?"
                            closeClick={this.close}
                            actions={(<div className="btn_wrapper">
                                <Button
                                    className="btn"
                                    text="Yes"
                                    handler={this.toggleCart}
                                    backgroundColor="transparent"
                                    borderColor="black"
                                    color="black"
                                />
                                <Button
                                    handler={this.close}
                                    className="btn"
                                    text="No"
                                    backgroundColor="transparent"
                                    borderColor="black"
                                    color="black"
                                />
                            </div>)}
                        /> :
                        (statusModal === "RemovingFromCart" ?
                                <Modal
                                    title="Removing from cart"
                                    text="Are you sure?"
                                    closeClick={this.close}
                                    actions={(<div className="btn_wrapper">
                                        <Button
                                            className="btn"
                                            text="Yes"
                                            handler={this.toggleCart}
                                            backgroundColor="transparent"
                                            borderColor="black"
                                            color="black"
                                        />
                                        <Button
                                            handler={this.close}
                                            className="btn"
                                            text="No"
                                            backgroundColor="transparent"
                                            borderColor="black"
                                            color="black"
                                        />
                                    </div>)}
                                /> :
                                ''
                        )
                }
            </div>
        )
    }
}

export default App;