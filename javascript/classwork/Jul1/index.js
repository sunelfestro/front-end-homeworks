// let user = {
//     name: 'John',
//     surname: 'Smith',
// }
//
// // user.name = 'Pete';
// console.log(user);
// delete user.name;
// console.log(user);
//
//
// function isEmpty(obj) {
//     for (let key in obj){
//         return false;
//     }
//     return true;
// }
//
// alert(isEmpty(user));

function calcYear(userAge){
    let today = new Date();
    let year = today.getFullYear() - userAge + 5508;
    return year;
}

function createUser() {
    let firstName = prompt("What's your name?");
    let secondName = prompt("Your last name:");
    let age = +prompt("Your age:");
    let gender = prompt("Your gender:");
    let obj = {
        name: firstName,
        firstLetter: firstName[0],
        last_name: secondName,
        age: age,
        gender: gender,
        year: calcYear(age),
        outputConsole: function conLog() {
            console.log(`Your name is ${this.last_name} ${this.firstLetter}., your gender is ${this.gender} & you born in ${this.year} year from Nativity`);
        }
    }
    return obj;
}

let user = createUser();
user.outputConsole();

