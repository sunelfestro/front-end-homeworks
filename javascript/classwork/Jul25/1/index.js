let timer = document.querySelector(".timer");
let btns = document.querySelectorAll(".btn");

let time = 1000;
let currentTimeState = 0;
let per;

let VeryHardTimeString = (currentTime) => {
    let minutes;
    let seconds;

    if(currentTime < 59){
        minutes = Math.floor(currentTime/60) < 10 ? "0" + Math.floor(currentTime/60) : Math.floor(currentTime/60);
        seconds = currentTime % 60 < 10 ? '0' + currentTime % 60 : currentTime;
    return `${minutes}:${seconds}`;
    }
    else {
        return `${minutes}:${currentTime % 60 < 10 ? '0' + currentTime : currentTime}`;
    }
}

let timerId = () => {
    currentTimeState += 1;
    let timeToStr = VeryHardTimeString(currentTimeState);
    timer.innerText = timeToStr;
};


btns[0].addEventListener("click", (e) => {
    e.target.innerText = "Track";
    per = setInterval(timerId, time);
})

btns[1].addEventListener("click", (e) => {
    btns[0].innerText = "Start";
    clearInterval(per);
})