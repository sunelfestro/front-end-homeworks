let pop_posts = document.querySelector(".page-posts");
let pop_clients = document.querySelector(".page-clients");
let top_rated = document.querySelector(".top-rated");
let hot_news = document.querySelector(".hot-news");

function getCoords(elem) {
    let box = elem.getBoundingClientRect();
    return box.top + pageYOffset;
}

function moveTo(crd){
    $("html, body").animate({
        scrollTop: getCoords(crd)
    }, 1000);
}

$(".scroll-btn").click(function(e){
    switch(e.target.innerText){
        case "MOST POPULAR POSTS":
            moveTo(pop_posts);
            break;

        case "OUR MOST POPULAR CLIENTS":
            moveTo(pop_clients);
            break;

        case "TOP RATED":
            moveTo(top_rated);
            break;

        case "HOT NEWS":
            moveTo(hot_news);
            break;
    }

})

$('.scrollup').click(function () {
    $("html, body").animate({
        scrollTop: 0
    }, 1000);
})

$(window).scroll(function () {
    if ($(this).scrollTop() > 200) {
        $('.scrollup').fadeIn();
    } else {
        $('.scrollup').fadeOut();
    }
});

$('.hide').click(function () {
    $('.posts_wrapper').slideToggle();
})

