function createMas() {
    let mas = [];
    for (let i = 0; i === i; i++) {
        mas[i] = prompt("STEP 1\n\nEnter element of massive or his type\n(press OK only to finish):");
        if (mas[i] === "") {
            mas.pop();
            break;
        }

        switch (mas[i].toLowerCase()) {
            case "null":
                mas[i] = null;
                break;

            case "undefined":
                mas[i] = undefined;
                break;

            case "function":
                mas[i] = () => {
                };
                break;

            case "true":
                mas[i] = true;
                break;

            case "false":
                mas[i] = false;
                break;

            case "object":
                mas[i] = {};
                break;

            default:
                break;
        }
        if (!(isNaN(parseInt(mas[i], 10)))) {
            mas[i] = parseInt(mas[i], 10);
        }
    }

    return mas;
}


function selectType() {
    let type = "";
    do {
        type = prompt("STEP 2\n\nEnter type of elements to filter:");
    } while (!(type.toLowerCase() === "object" || type.toLowerCase() === "number" || type.toLowerCase() === "null" || type.toLowerCase() === "undefined" || type.toLowerCase() === "boolean" || type.toLowerCase() === "function" || type.toLowerCase() === "string"));
    return type.toLowerCase();
}


function filterBy(mass, typ) {
    let filterMass;

    if (typ === "null") {
        return filterMass = [];
    }

    return filterMass = mass.filter((n) => {
        return typeof n === typ
    });
}


let massive = createMas();
let type = selectType();
let filterMassive = filterBy(massive, type);
console.log("Your massive:");
console.log(massive);
console.log(`Filter type: ${type}`);
console.log("Filtered massive:");
if (type === "null") {
    console.log("Massive with null only is empty");
}
console.log(filterMassive);