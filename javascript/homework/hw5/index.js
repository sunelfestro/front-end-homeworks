function createNewUser() {
    let newUser = {
        firstName: prompt("What's your name?", ""),
        lastName: prompt("What's your surname?", ""),
        birthday: prompt("Your date of birth:", ""),

        getAge: function() {
            let getDay = +this.birthday.substr(0, 2);
            let getMonth = +this.birthday.substr(3,2) - 1;
            let getYear = +this.birthday.substr(-4);
            let dateOfBirth = new Date(getYear, getMonth, getDay);

            let today = new Date();
            let age = today.getFullYear() - dateOfBirth.getFullYear();
            let compareData = new Date (today.getFullYear(), getMonth, getDay);
            if (compareData.getTime() > today.getTime()){
                age -= 1;
            }

            return age;
        },

        fullName: function getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },

        getPassword: function() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substr(-4);
        }
    }
    return newUser;
}

let user = createNewUser();
console.log(user);
console.log (user.getAge());
console.log(user.getPassword());