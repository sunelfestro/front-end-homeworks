let imgs = document.querySelectorAll(".img");
let btns = document.querySelectorAll(".btn");

let charge = () => {
    imgs.forEach((elem, i) => {
        if (elem.classList.contains("image-show")) {
            setTimeout(() => {
                if (i === imgs.length - 1) {
                    elem.classList.remove("image-show");
                    imgs[0].classList.add("image-show");
                }
                elem.classList.remove("image-show");
                elem.nextElementSibling.classList.add("image-show");
            });
        }
    })
};

let period = setInterval(charge,1000);

btns[0].addEventListener('click', (e) => {
    e.preventDefault();
    clearInterval(period);
});

btns[1].addEventListener('click', (e) => {
    e.preventDefault();
    period = setInterval(charge,1000);
});