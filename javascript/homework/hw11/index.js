function event(i) {
    btns.forEach((n) => {
        if (n.classList.contains('btn-event')) {
            n.classList.remove('btn-event');
            n.classList.add('btn-bg');
        }
    })
    btns[i].classList.remove("btn-bg");
    btns[i].classList.add("btn-event");
}

let btns = document.querySelectorAll('.btn');

document.addEventListener('keydown', (e) => {
        switch (e.code) {
            case "Enter":
                event(0);
                break;

            case "KeyS":
                event(1);
                break;

            case "KeyE":
                event(2);
                break;

            case "KeyO":
                event(3);
                break;

            case "KeyN":
                event(4);
                break;

            case "KeyL":
                event(5);
                break;

            case "KeyZ":
                event(6);
                break;
        }
    }
)