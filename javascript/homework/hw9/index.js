function removeContent() {
    content.forEach((item) => {
        item.style.display = 'none';
    })
}

function showContent(i) {
    content[i].style.display = 'block';
}

function removeActiveTab() {
    tabs.forEach((elem) => {
        elem.classList.remove('active');
    })
}


let menu = document.querySelector('.tabs');
let tabs = document.querySelectorAll('.tabs-title');
let content = document.querySelectorAll('.content-text');

removeContent();
showContent(0);

menu.addEventListener('click', (e) => {
    let tg = e.target;
    removeActiveTab();

    if (tg.classList.contains("tabs-title")) {
        tabs.forEach((tab, i) => {
            if (tg === tab) {
                removeContent();
                tab.classList.add('active');
                content.forEach((item, i) => {
                    if (tg.innerText === content[i].id) {
                        showContent(i);
                    }
                })
            }
        })
    }
})