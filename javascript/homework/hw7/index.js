function createList(mas) {
    let list = document.createElement("ul");

    let listMas = mas.map((n) => {
        if (typeof n === "string" || typeof n === "number") {
            let li = document.createElement("li");
            li.innerText = n;
            return li;
        } else {
            return false;
        }
    })

    listMas.forEach((elem) => {
        if (elem) {
            list.append(elem);
        }
    })

    document.querySelector('body').prepend(list);
}

let massive = ['Kiev', 23, null, "Yurta", 322, "Dnipro", [], {}];
createList(massive);