function calculator(fst, oper, sec){
    let result = 0;
    switch (oper) {
        case "+":
            result = fst + sec; // плюс
            break;

        case "-":
            result = fst - sec; // минус
            break;

        case "*":
            result = fst * sec; // умножение
            break;

        case "/":
            result = fst / sec; // деление
            break;

        default:
            alert("Invalid operation, returning 0..."); //если пользователь ввёл что-то не то в операции
    }
    return result;
}

let first = +prompt("Enter first number:", ``);
let operation = prompt("Enter your operation:", ``);
let second = +prompt("Enter second number:", ``);

//если пользователь ввёл что-то не то в числа
if (isNaN(first) || isNaN(second)){
    alert("No numbers!");
}
else{
    console.log(`${first} ${operation} ${second} = ${calculator(first, operation, second)}`); //вывод на консоль результата
}

