let eyes = document.querySelectorAll(".icon-password");
let poles = document.querySelectorAll(".input");
let btn = document.querySelector("button");

btn.addEventListener('click', (e) => {
    e.preventDefault();

    if(document.querySelector(".error-text").style.display === 'block'){
        document.querySelector(".error-text").style.display = 'none';
    }

    if (poles[0].value === poles[1].value && poles[0].value !== ""){
        alert('You\`re welcome!');
    }
    else {
        document.querySelector(".error-text").style.display = 'block';
    }
})

eyes.forEach((i, num) => {
    i.addEventListener('click', (e) => {
        let tg = e.target;
        if (tg.classList.contains('fa-eye')){
            poles[num].type = "password";
            tg.classList.remove('fa-eye');
            tg.classList.add('fa-eye-slash');
        }
        else {
            poles[num].type = "text";
            tg.classList.remove('fa-eye-slash');
            tg.classList.add('fa-eye');
        }
    })
})
