let pole = document.querySelector("input");
let price = document.querySelector(".alert");
let priceText = price.querySelector("span").textContent;
let error = document.querySelector(".error-text");
let button = document.querySelector(".close-button")
let priceNumber;

button.addEventListener("click", () => {
    price.style.display = "none";
    priceText = "Текущая цена: $";
    pole.value = "";
})

pole.addEventListener('focus', () => {
    pole.value = "";
    if (pole.classList.contains('pole-error')){
        pole.classList.remove('pole-error');
    }
    if(price.classList.contains('alert-blur')){
        priceText = "Текущая цена: $";
        price.classList.remove('alert-blur');
        pole.value = "";
    }
    if(error.style.display === "inline"){
        error.style.display = "none";
    }
    pole.classList.add('pole-focus');
});

pole.addEventListener('blur', () => {
    pole.classList.remove('pole-focus');
    if(isNaN(pole.valueAsNumber)){
        pole.classList.add("pole-error");
        error.style.display = "inline";
    }
    else{
        priceNumber = pole.valueAsNumber;
    }

    if (priceNumber <= 0) {
        pole.classList.add("pole-error");
        error.style.display = "inline";
    } else if(!isNaN(pole.valueAsNumber)){
        priceText += `${priceNumber}`;
        pole.classList.add('pole-blur');
        price.querySelector("span").textContent = priceText;
        price.classList.add('alert-blur');
    }
});