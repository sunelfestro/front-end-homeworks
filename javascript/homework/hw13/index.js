let imgs = document.querySelectorAll(".img");
let btns = document.querySelectorAll(".btn");
let body = document.querySelector('body');
let link = document.querySelectorAll('link');

btns[1].style.display = "none";
console.log(link);

let charge = () => {
    imgs.forEach((elem, i) => {
        if (elem.classList.contains("image-show")) {
            setTimeout(() => {
                if (i === imgs.length - 1) {
                    elem.classList.remove("image-show");
                    imgs[0].classList.add("image-show");
                }
                elem.classList.remove("image-show");
                elem.nextElementSibling.classList.add("image-show");
            });
        }
    })
};

let period = setInterval(charge,1000);

btns[0].addEventListener('click', (e) => {
    e.preventDefault();
    clearInterval(period);
    btns[0].style.display = 'none';
    btns[1].style.display = 'block';
});

btns[1].addEventListener('click', (e) => {
    e.preventDefault();
    period = setInterval(charge,1000);
    btns[1].style.display = 'none';
    btns[0].style.display = 'block';
});

if(!(localStorage.getItem("theme") === "")){
    link[1].href = localStorage.getItem("theme");
    localStorage.clear();
}

btns[2].addEventListener('click', (e) => {
    e.preventDefault();
    let stl = link[1];
    localStorage.setItem("theme", "");

    if(stl.href === "http://localhost:63342/front-end-homeworks/javascript/homework/hw13/css/main-theme-1.css"){
        stl.href = "http://localhost:63342/front-end-homeworks/javascript/homework/hw13/css/main-theme-2.css";
        localStorage.setItem("theme", stl.href);
    }
    else if(stl.href === "http://localhost:63342/front-end-homeworks/javascript/homework/hw13/css/main-theme-2.css"){
        stl.href = "http://localhost:63342/front-end-homeworks/javascript/homework/hw13/css/main-theme-3.css";
        localStorage.setItem("theme", stl.href);
    }
    else if(stl.href === "http://localhost:63342/front-end-homeworks/javascript/homework/hw13/css/main-theme-3.css"){
        stl.href = "http://localhost:63342/front-end-homeworks/javascript/homework/hw13/css/main-theme-1.css";
        localStorage.setItem("theme", stl.href);
    }
});