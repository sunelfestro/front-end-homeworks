class Employee {
    constructor(obj) {
        this.name = obj.name;
        this.age = obj.age;
        this.salary = obj.salary;
    }

    get nameInfo() {
        return this.name;
    }

    get ageInfo() {
        return this.age;
    }

    get salaryInfo() {
        return this.salary;
    }

    set nameInfo(name) {
        this.name = name;
    }

    set ageInfo(age) {
        this.age = age;
    }

    set salaryInfo(salary) {
        this.salary = salary;
    }
}

class Programmer extends Employee {
    constructor(obj) {
        super(obj);
        this.lang = obj.lang;
    }

    get salaryInfo() {
        return this.salary * 3;
    }
}

let pr1 = new Programmer({
    name: 'Andrew',
    age: 27,
    salary: 3000,
    lang: 'eng, deu'
});

let pr2 = new Programmer({
    name: 'Boris',
    age: 12,
    salary: 300,
    lang: 'rus'
});

let pr3 = new Programmer({
    name: 'Ruud',
    age: 35,
    salary: 5000,
    lang: 'eng, deu, fr, pol'
});

console.log(pr1);
console.log(pr2);
console.log(pr3);