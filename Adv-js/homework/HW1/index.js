// Задание №1
const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

let clients3 = new Set([...clients1, ...clients2]);
let newClients = [...clients3];
console.log("\n#1:");
console.log(newClients);

//Задание №2
let characters = [
    {
        name: "Елена",
        lastName: "Гилберт",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Кэролайн",
        lastName: "Форбс",
        age: 17,
        gender: "woman",
        status: "human"
    },
    {
        name: "Аларик",
        lastName: "Зальцман",
        age: 31,
        gender: "man",
        status: "human"
    },
    {
        name: "Дэймон",
        lastName: "Сальваторе",
        age: 156,
        gender: "man",
        status: "vampire"
    },
    {
        name: "Ребекка",
        lastName: "Майклсон",
        age: 1089,
        gender: "woman",
        status: "vempire"
    },
    {
        name: "Клаус",
        lastName: "Майклсон",
        age: 1093,
        gender: "man",
        status: "vampire"
    }
];

let newCharacters = characters.map((elem) => {
   let {name, lastName, age} = elem;
   return {name, lastName, age};
})

console.log("\n#2:");
console.log(newCharacters);


//Задание №3
const user1 = {
    name: "John",
    years: 30
};

let {name, years, isAdmin = false} = user1;
console.log("\n#3:");
console.log(name);
console.log(years);
console.log(isAdmin);


//Задание №4
const satoshi2020 = {
    name: 'Nick',
    surname: 'Sabo',
    age: 51,
    country: 'Japan',
    birth: '1979-08-21',
    location: {
        lat: 38.869422,
        lng: 139.876632
    }
}

const satoshi2019 = {
    name: 'Dorian',
    surname: 'Nakamoto',
    age: 44,
    hidden: true,
    country: 'USA',
    wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    browser: 'Chrome'
}

const satoshi2018 = {
    name: 'Satoshi',
    surname: 'Nakamoto',
    technology: 'Bitcoin',
    country: 'Japan',
    browser: 'Tor',
    birth: '1975-04-05'
}

const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
console.log("\n#4:");
console.log(fullProfile);


//Задание №5
const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
}, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
}, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

const newBooks = [...books, bookToAdd];
console.log("\n#5:");
console.log(newBooks);


//задание №6
const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

let age = 30,
    salary = 3000,
    newEmployee = {...employee, age, salary};
console.log("\n#6:");
console.log(newEmployee);


//Задание №7
const array = ['value', () => 'showValue'];

const [value, showValue] = array;

alert(value);
alert(showValue());