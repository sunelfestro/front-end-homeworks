function createBook (book) {
    let listMember = document.createElement("li");
    listMember.innerText = `Author: ${book.author}\nName: ${book.name}\nPrice: ${book.price}`;
    list.append(listMember);
};

function deleteBook() {
    list.removeChild(list.lastChild);
}


const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


let body = document.querySelector("body");
let root = document.createElement("div");
root.id = "root";
let list = document.createElement("ul");
body.prepend(root);
root.prepend(list);


books.forEach((elem, i) => {
    createBook(elem);

    try {
        if (!("author" in elem)) {
            throw new Error (`Book #${i+1}: invalid author`);
        }
    } catch (err) {
        deleteBook();
        console.log(err);
    }

    try {
        if (!("name" in elem)) {
            throw new Error (`Book #${i+1}: invalid name`);
        }
    } catch (err) {
        deleteBook();
        console.log(err);
    }

    try {
        if (!("price" in elem)) {
            throw new Error (`Book #${i+1}: invalid price`);
        }
    } catch (err) {
        deleteBook();
        console.log(err);
    }
});
